$(function() {
  $('.bistro').parallax("0%", 0.2);
  $('.bistro > div').parallax("2%", 1.2);
  $('a[href^="#"]').on('click',
    function() { $.scrollTo($(this).attr('href'), 1000); }
  );

  if ($('.textcell.tablette').length) {
    setInterval(function() {
      var $clock = $('.mobile-case .clock');
      var date = new Date();
      $clock.text(('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2));
    }, 500);
  }

  $('.textcell.tablette img').each(function(idx, elem) {
    var $cell = $(elem).parents('div.textcell');
    var $mobile_case = $('<div class="mobile-case">' +
                           '<div class="screen">' +
                             '<div class="mobile-top-bar"><span class="clock">--:--</span></div>' +
                             '<div class="mobile-app-content">' +
                             '</div>' +
                           '</div>' +
                         '</div>');
    $mobile_case.find('.mobile-app-content').append($(elem).detach());
    $cell.append($mobile_case);
  });

  if ($('.ecrans').length > 0) {
    var $window = $(window);
    var offset = parseInt(Math.random()*2000);
    var $ecrans = $('.ecrans');
    $window.bind('scroll', function() {
      var pos = Math.pow($window.scrollTop(), 1.1);
      $ecrans.css('background-position', (offset + pos) + 'px 0px');
    });
    $window.trigger('scroll');
  }
  if ($('div[id|=chiffres]').length) {
    var $chiffres_section = $('div[id|=chiffres]');
    $chiffres_section.css('position', 'relative');
    var total = $chiffres_section.attr('id').split('-')[1];
    var $counter = $('<span id="counter"></span>').appendTo($chiffres_section.find('> div'));
    var options = {
      useEasing : true,
      useGrouping : true,
      separator : ' ',
      prefix : '',
      suffix : ' demandes actives'
    };
    function get_current_count() {  // (saas only)
      var base_number = 13785600;
      var base_date = new Date("2024-01-01T00:00:00");
      var diff = (Date.now() - base_date.getTime()) / 1000;
      /* approx rate on base date,
       * January 1th: 13785600, March 1st: 14711000
       * 1 / ((14711000 - 13785600) / (86400seconds * 60days)) -> 5.6
       */
      return base_number + (diff / 5.6);
    }
    //var count = new CountUp("counter", 1234, parseInt(total), 0, 4.5, options);
    var count = new CountUp("counter", 1, parseInt(get_current_count()), 0, 4.5, options);
    $chiffres_section.on('inview', function() {
      count.start();
    });
    setInterval(function() {
      count.update(get_current_count());
    }, 2000);
  }
});
