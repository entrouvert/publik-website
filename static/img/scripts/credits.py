#! /usr/bin/env python

import xml.etree.ElementTree as ET
import os

print '<ul>'
for filepath in sorted(os.listdir('sources')):
    if not filepath.endswith('.svg'):
        continue
    tree = ET.fromstring(file('sources/' + filepath).read())
    author = None
    for elem in tree.getchildren():
        if elem.tag == '{http://www.w3.org/2000/svg}text' and elem.text.startswith('Created by'):
            author = elem.text[len('Created by')+1:]
            tree.remove(elem)
            break
    else:
        continue
    print '<li>%s, par %s de <a href="http://thenounproject.com/">The Noun Project</a></li>' % (
            filepath.split('.')[0].title(), author)
print '</ul>'
