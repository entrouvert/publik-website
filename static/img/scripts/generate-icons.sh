#! /bin/sh

python generate.py --width 160 --colour 888888 citoyens.svg metier.svg parametrage.svg
python generate.py --width 160 --colour aaaaaa libre.svg data.svg

python generate.py --width 80 --colour 888888 compte-citoyen.svg demarches.svg mobilite.svg porte-document.svg cellules.svg
python generate.py --width 80 --colour 888888 administration.svg bi.svg federation-identite.svg webservices.svg
python generate.py --width 80 --colour 888888 annonces.svg cms.svg fabrique-formulaires.svg guichet-unique.svg fabrique-workflows.svg


python generate.py --width 160 --colour cccccc --variant bg compte-citoyen.svg
python generate.py --width 160 --colour cccccc --variant bg mobilite.svg
python generate.py --width 160 --colour cccccc --variant bg guichet-unique.svg
python generate.py --width 160 --colour cccccc --variant bg fabrique-workflows.svg
python generate.py --width 160 --colour cccccc --variant bg cms.svg
python generate.py --width 160 --colour cccccc --variant bg administration.svg
python generate.py --width 160 --colour cccccc --variant bg federation-identite.svg

python generate.py --width 160 --colour 333333 --variant bg demarches.svg
python generate.py --width 160 --colour 333333 --variant bg porte-document.svg
python generate.py --width 160 --colour 333333 --variant bg fabrique-formulaires.svg
python generate.py --width 160 --colour 333333 --variant bg annonces.svg
python generate.py --width 160 --colour 333333 --variant bg webservices.svg
python generate.py --width 160 --colour 333333 --variant bg bi.svg
python generate.py --width 160 --colour 333333 --variant bg cellules.svg

