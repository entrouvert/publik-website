#! /usr/bin/env python

import os
import subprocess
import sys

captures = [
    ('https://cournonterral.test-eservices.montpellier3m.fr/',
     'cournonterral.png'),
    ('https://fabregues.eservices.montpellier3m.fr/',
     'fabregues.png'),
    ('https://marcoussis.au-quotidien.com/etat-civil/copie-d-acte-de-naissance/',
     'marcoussis.png'),
    ('https://meyzieu.au-quotidien.com/inscriptions/ramassage-des-accueils-de-loisirs-du-mercredi-inscription-mensuelle-1/',
     'meyzieu.png'),
    ('https://eservices.montpellier3m.fr/',
     'montpellier.png'),
    ('https://sussargues.test-eservices.montpellier3m.fr/',
     'sussargues.png'),
    ('https://vendargues.test-eservices.montpellier3m.fr/',
     'vendargues.png'),
    ('https://demarches.vincennes.fr/question/votre-question-jeunesse/',
     'vincennes.png'),
    ('https://perols.eservices.montpellier3m.fr/',
     'perols.png'),
    ('https://espace-usager-alfortville.test.entrouvert.org/',
     'alfortville.png'),
    ('https://citoyen-fondettes.test.entrouvert.org/',
     'fondettes.png'),
    ('https://mesdemarches.fontenay-sous-bois.fr/',
     'fontenay-sous-bois.png'),
    ('https://www.mytournai.be/',
     'tournai.png'),
    ('https://citoyen.chateauroux-metropole.fr/',
     'chateauroux.png'),
    ('https://nancy.test.entrouvert.org/',
     'nancy.png'),
    ('https://la-seyne.sictiam.dev.entrouvert.org/',
     'la-seyne-sur-mer.png'),
    ('https://services.strasbourg.eu/',
     'strasbourg.png'),
    ('https://blois.test.entrouvert.org/',
     'blois.png'),
    ]

svg_src = open('screen-with-capture.svg').read()
for url, filename in captures:
    print url
    if '--reshoot' in sys.argv or not os.path.exists('captures/%s' % filename):
        print 'creating capture'
        subprocess.call(['phantomjs', '--ssl-protocol=any',
            '--ignore-ssl-errors=yes', 'capture.js', url, 'captures/%s' % filename])

    print 'putting capture in a mini screen'
    svg_dst = svg_src.replace('xxx.png', 'captures/%s' % filename)
    svg_path = open('/tmp/.xxx.svg', 'w')
    svg_path.write(svg_dst)
    svg_path.close()
    subprocess.call(['inkscape', '--without-gui',
        '--file', '/tmp/.xxx.svg',
        '--export-area-drawing',
        '--export-area-snap',
        '--export-png', 'screen-%s' % filename,
        '--export-width', '250'])

filenames = [x[1] for x in captures]


# gm montage screen-*.png -geometry +10+0 -tile 8x1 -background none ../screens.png
# +10+0 <- 10 is the margin
# 8x1   <- 8 is the number of images
print 'assembling mini screens'
subprocess.call(['gm', 'montage'] + ['screen-%s' % x for x in filenames] +
    ['-geometry', '+10+0', '-tile', '%sx1' % len(captures),
     '-background', 'none', '../../screens.png'])
