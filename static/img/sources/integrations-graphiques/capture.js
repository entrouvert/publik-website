var page = require('webpage').create();
var system = require('system');

var address = system.args[1];
var filename = system.args[2];

page.viewportSize = { width: 1320, height: 768 };
page.clipRect = { top: 0, left: 0, width: page.viewportSize.width, height: page.viewportSize.height };
page.open(address, function() {
    page.render(filename);
    phantom.exit();
});
